package org.alpana.jms.p2p.flightmgmt.reservation.listeners;

import org.alpana.jms.p2p.flightmgmt.model.Passenger;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

public class ReservationListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        ObjectMessage requestMessage = (ObjectMessage) message;

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()) {

            Passenger passenger = (Passenger) requestMessage.getObject();
            String passengerId = passenger.getId();
            String passengerLastName = passenger.getLastName();
            String passengerfirstName = passenger.getFirstName();
            String passengerEmail = passenger.getEmail();
            String passengerPhone = passenger.getPhone();
            System.out.println("----------------- Passenger Details -----------------");
            System.out.println("ID: " + passengerId);
            System.out.println("First Name: " + passengerfirstName);
            System.out.println("Last Name: " + passengerLastName);
            System.out.println("Email: " + passengerEmail);
            System.out.println("Phone: " + passengerPhone);

            JMSProducer producer = jmsContext.createProducer();
            MapMessage replyMessage = jmsContext.createMapMessage();
            if (passengerId.length() == 10 &&
                !passengerLastName.isEmpty() &&
                !passengerfirstName.isEmpty() &&
                EmailValidator.getInstance().isValid(passengerEmail)
            ){
                replyMessage.setBoolean("reservation", true);
                replyMessage.setString("id", RandomStringUtils.randomAlphanumeric(15));
            }
             else {
                replyMessage.setBoolean("reservation", false);
            }

            replyMessage.setJMSCorrelationID(requestMessage.getJMSMessageID());
            producer.send(requestMessage.getJMSReplyTo(), replyMessage);

        } catch (JMSException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}
