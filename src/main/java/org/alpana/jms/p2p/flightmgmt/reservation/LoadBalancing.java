package org.alpana.jms.p2p.flightmgmt.reservation;

import org.alpana.jms.p2p.flightmgmt.model.Passenger;
import org.alpana.jms.p2p.flightmgmt.reservation.listeners.ReservationListener;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class LoadBalancing {

    public static void main(String[] args) throws NamingException, JMSException {

        InitialContext initialContext = new InitialContext();
        Queue requestQueue = (Queue) initialContext.lookup("queue/requestQueue");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()){
            JMSConsumer consumer1 = jmsContext.createConsumer(requestQueue);
            JMSConsumer consumer2 = jmsContext.createConsumer(requestQueue);
            for (int i = 0; i < 5; i++) {
                ObjectMessage receivedMessage1 = (ObjectMessage) consumer1.receive();
                ObjectMessage receivedMessage2 = (ObjectMessage) consumer1.receive();
                Passenger passenger1 = (Passenger) receivedMessage1.getObject();
                Passenger passenger2 = (Passenger) receivedMessage2.getObject();
                System.out.println("----------------- From Consumer 1 Passenger Details -----------------");
                System.out.println("ID: " + passenger1.getId());
                System.out.println("First Name: " + passenger1.getFirstName());
                System.out.println("Last Name: " + passenger1.getLastName());
                System.out.println("Email: " + passenger1.getEmail());
                System.out.println("Phone: " + passenger1.getPhone());
                System.out.println("----------------- From Consumer 2 Passenger Details -----------------");
                System.out.println("ID: " + passenger2.getId());
                System.out.println("First Name: " + passenger2.getFirstName());
                System.out.println("Last Name: " + passenger2.getLastName());
                System.out.println("Email: " + passenger2.getEmail());
                System.out.println("Phone: " + passenger2.getPhone());
                System.out.println("---------------------------------------------------------------------");
            }
        }
    }
}
