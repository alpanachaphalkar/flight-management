package org.alpana.jms.p2p.flightmgmt.checkin;

import org.alpana.jms.p2p.flightmgmt.model.Passenger;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.commons.lang3.RandomStringUtils;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class App {

    public static void main(String[] args) throws NamingException, JMSException {
        InitialContext initialContext = new InitialContext();
        Queue requestQueue = (Queue) initialContext.lookup("queue/requestQueue");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()){
            JMSProducer producer = jmsContext.createProducer();
            TemporaryQueue replyQueue = jmsContext.createTemporaryQueue();

            ObjectMessage requestMessage = jmsContext.createObjectMessage();
            Passenger passenger = new Passenger();
            passenger.setId(RandomStringUtils.randomAlphanumeric(10));
            passenger.setFirstName("Virginia");
            passenger.setLastName("Woolf");
            passenger.setEmail("virginia.woolf@xyz.gb");
            passenger.setPhone("+441234567890");
            requestMessage.setObject(passenger);

            requestMessage.setJMSReplyTo(replyQueue);
            producer.send(requestQueue, requestMessage);
            System.out.println("Request Message ID: " + requestMessage.getJMSMessageID());
            Map<String, ObjectMessage> requestMessages = new HashMap<>();
            requestMessages.put(requestMessage.getJMSMessageID(), requestMessage);

            JMSConsumer consumer = jmsContext.createConsumer(replyQueue);
            MapMessage replyMessage = (MapMessage) consumer.receive(30000);
            System.out.println("Reply Message Correlation ID: " + replyMessage.getJMSCorrelationID());
            Passenger reqPassenger = (Passenger) requestMessages.get(replyMessage.getJMSCorrelationID()).getObject();
            System.out.println("----------------- Passenger Details -----------------");
            System.out.println("ID: " + reqPassenger.getId());
            System.out.println("First Name: " + reqPassenger.getFirstName());
            System.out.println("Last Name: " + reqPassenger.getLastName());
            System.out.println("Email: " + reqPassenger.getEmail());
            System.out.println("Phone: " + reqPassenger.getPhone());
            System.out.println("-----------------------------------------------------");
            Boolean isReserved = replyMessage.getBoolean("reservation");
            if(isReserved){
                System.out.println("Reservation completed with ID: " + replyMessage.getString("id"));
            } else {
                System.out.println("Reservation cannot be completed because of invalid user data.");
            }

        }
    }
}
