package org.alpana.jms.p2p.flightmgmt.checkin;

import org.alpana.jms.p2p.flightmgmt.model.Passenger;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.commons.lang3.RandomStringUtils;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LoadBalancing {

    public static void main(String[] args) throws NamingException, JMSException {

        InitialContext initialContext = new InitialContext();
        Queue requestQueue = (Queue) initialContext.lookup("queue/requestQueue");

        try(ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory();
            JMSContext jmsContext = cf.createContext()){
            JMSProducer producer = jmsContext.createProducer();

            Passenger[] passengers = new Passenger[10];
            for (int i = 0; i < passengers.length; i++) {
                passengers[i] = new Passenger();
                passengers[i].setId(RandomStringUtils.randomAlphanumeric(10));
                passengers[i].setFirstName(RandomStringUtils.randomAlphabetic(5).toLowerCase());
                passengers[i].setLastName(RandomStringUtils.randomAlphabetic(8).toLowerCase());
                passengers[i].setEmail(RandomStringUtils.randomAlphabetic(12).toLowerCase()
                        + "@" + RandomStringUtils.randomAlphabetic(4, 5).toLowerCase()
                        + "." + RandomStringUtils.randomAlphabetic(2,3).toLowerCase());
                passengers[i].setPhone("+" + RandomStringUtils.randomNumeric(2,3) + RandomStringUtils.randomNumeric(7,10));
                ObjectMessage requestMessage = jmsContext.createObjectMessage();
                requestMessage.setObject(passengers[i]);
                producer.send(requestQueue, requestMessage);
            }
        }
    }
}
